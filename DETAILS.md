# Laravel Many To Many
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-many-to-many.git`
2. Go inside the folder: `cd laravel-many-to-many`
3. Set your desired database name in `.env` file
4. Run `laravel_ManyToMany.sql` file in your database.
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000/anggota

### Screen shot

Table Relationship

![Table Relationship](img/relasi.png "Table Relationship")

Anggota Hadiah Page

![Anggota Hadiah Page](img/anggota.png "Anggota Hadiah Page") 
